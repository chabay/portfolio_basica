{{--
  ./resources/views/pages/show.blade.php
  variables disponibles :
      - $page Page
      Page 1 (home)
        - $projets array(Projet)
        - $slides array(Projet)
        - $posts array(Post)
        - $tweets array(Twitter)
      Page 2 (portfolio)
        - $projets array(Projet)
      Page 3 (blog)
        - $posts array(Post)
 --}}

@extends('template.defaut')

@section('title')

  @if ($page->id === 1)
    BASICA!
  @else
    {{ $page->titrePage}}
  @endif

@endsection

@section('content1')

  {{-- ACCUEIL --}}
  @if ($page->id === 1)
    <!-- Slider -->
    @include('projets.slider')
    <!-- Derniers projets -->
    @include('projets.latest')

    <!-- Our Articles -->
    <div class="section">
      <div class="container">
        <div class="row">
            <!-- Derniers posts du blog -->
            @include('posts.latest')
            <!-- Derniers tweets -->
            @include('tweets.latest')
          <!-- End Featured News -->
       </div>
     </div>
    </div>

    {{-- Scripts de la page d'accueil--}}
     @section('scripts')
       <script src="{{ asset('js/app.js') }}"> </script>
     @endsection
   {{-- FIN ACCUEIL --}}

   {{-- HEADER DES AUTRES PAGES --}}
   @else
    <!-- Page Title -->
    <div class="section section-breadcrumbs">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1>{{ $page->titrePage}}</h1>
          </div>
        </div>
      </div>
    </div>

    {{-- PORTFOLIO --}}
    @if ($page->id === 2)
      <div class="section">
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <h2>{{ $page->titreContenu }}</h2>
              <h3>{{ $page->sousTitreContenu }}</h3>
              <p>
                {{ $page->texte }}
              </p>
            </div>
          </div>
        </div>
      </div>
      @include('projets.index')
      @section('scripts')
        <script src="{{ asset('js/projets/older.js') }}"> </script>
      @endsection
    {{-- FIN PORTFOLIO --}}

    {{-- BLOG --}}
    @elseif ($page->id === 3)
      @include('posts.index')
    {{-- FIN BLOG --}}

    {{-- CONTACT --}}
    @elseif ($page->id === 4)
      @include('template.partials._contact')
    {{-- FIN CONTACT --}}
    @endif

  {{-- FIN DES HEADERS --}}
@endif

@endsection
