{{--
  ./resources/views/pages/_menu.blade.php
  variables disponibles :
      - $pages array(Page)
 --}}


 <ul class="nav navbar-nav navbar-right">
   @foreach ($pages as $page)
     @if (request()->segment(1) == 'categories' or request()->segment(1) == 'posts')
       <li class="{{ $page->id == 3 ? 'active' : '' }}">
         <a href="{{ route('pages.show', [
           'page' => $page->id,
           'slug' => Str::slug($page->titreMenu)
         ]) }}">
         {{ $page->titreMenu }}
         </a>
       </li>
     @elseif (request()->segment(1) == 'tags' or request()->segment(1) == 'projets')
       <li class="{{ $page->id == 2 ? 'active' : '' }}">
         <a href="{{ route('pages.show', [
           'page' => $page->id,
           'slug' => Str::slug($page->titreMenu)
         ]) }}">
         {{ $page->titreMenu }}
         </a>
       </li>
     @else
       <li class="{{ request()->segment(1) == 'pages' && request()->segment(2) == $page->id ? 'active' : '' }}">
         <a href="{{ route('pages.show', [
           'page' => $page->id,
           'slug' => Str::slug($page->titreMenu)
         ]) }}">
         {{ $page->titreMenu }}
         </a>
       </li>
     @endif
   @endforeach
 </ul>
