{{--
  ./resources/views/tweets/latest.blade.php
  variables disponibles :
      - $tweets array(Twitter)
 --}}


<!-- Latest News Twitter -->
<div class="col-sm-6 latest-news">
  <h2>Latest Twitter News</h2>
  @foreach ($tweets as $tweet)
  <div class="row">
    <div class="col-sm-12">
      <div class="caption"><a href="{{ Twitter::linkUser($tweet->user->screen_name) }}">{{ $tweet->user->name . ' @' . $tweet->user->screen_name}}</a></div>
      <div class="date">{{ \Carbon\Carbon::parse($tweet->created_at)->format('d F Y H i') }}</div>
      <div class="intro">
        {!! Twitter::linkify($tweet->text); !!}
        <br/>
        <a href="{{ Twitter::linkUser($tweet->user->screen_name . '/status/' . $tweet->id ) }}">Read more...</a>
      </div>
    </div>
  </div>
  @endforeach
</div>
