{{--
  ./resources/views/projets/index.blade.php
  variables disponibles :
      - $projets array(Projet)
 --}}


 <div class="section">
   <div class="container">
     <div class="row">
       <ul class="grid cs-style-3" id="liste-projets">
         @include('projets.list')
       </ul>
     </div>
     @if (count($projets) === 6)
      <ul class="pager">
       <li><a href="#" id="load-more">More works</a></li>
      </ul>
     @endif
   </div>
 </div>
