{{--
  ./resources/views/projets/latest.blade.php
  variables disponibles :
      - $projets array(Projet)
 --}}

 <!-- Our Portfolio -->
     <div class="section section-white">
       <div class="container">
         <div class="row">

           <div class="section-title">
             <h1>Our Recent Works</h1>
           </div>

             <ul class="grid cs-style-3">
              @include('projets.list')
             </ul>

         </div>
       </div>
     </div>
  <!-- Our Portfolio -->
  <hr>
