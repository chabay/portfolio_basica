{{--
  ./resources/views/projets/list.blade.php
  variables disponibles :
      - $projets array(Projet)
 --}}


 @foreach ($projets as $projet)
  <div class="col-md-4 col-sm-6 projet">
   <figure>
     <img src="{{ asset('img/portfolio/' . $projet->image) }}" alt="{{ $projet->titre }}">
     <figcaption>
       <h3>{{ $projet->titre }}</h3>
       <span>{{ $projet->client }}</span>
       <a href="{{ route('projets.show', [
         'projet' => $projet->id,
         'slug' => Str::slug($projet->titre)
       ]) }}">Take a look</a>
     </figcaption>
   </figure>
  </div>
 @endforeach
