{{--
  ./resources/views/projets/slider.blade.php
  variables disponibles :
      - $slides array(Projet)
 --}}

<section id="main-slider" class="no-margin">
    <div class="carousel slide">
        <ol class="carousel-indicators">
          @foreach ($slides as $slide)
            <li data-target="#main-slider" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
          @endforeach
        </ol>
        <div class="carousel-inner">
          @foreach ($slides as $slide)
            <div class="item {{ $loop->first ? ' active' : '' }}" style="background-image: url({{ asset('img/portfolio/'. $slide->image) }}">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="carousel-content centered">
                                <h2 class="animation animated-item-1">{{ $slide->titre }}</h2>
                                <p class="animation animated-item-2">{{ Str::words($slide->description, 30, '...') }}</p>
                            </div>
                            <br>
                            <a class="btn btn-md animation animated-item-3" href="{{ route('projets.show', [
                              'projet' => $slide->id,
                              'slug' => Str::slug($slide->titre)
                            ]) }}">Learn More</a>
                        </div>
                    </div>
                </div>
            </div><!--/.item-->
          @endforeach
        </div><!--/.carousel-inner-->
    </div><!--/.carousel-->
    <a class="prev hidden-xs" href="#main-slider" data-slide="prev">
        <i class="icon-angle-left"></i>
    </a>
    <a class="next hidden-xs" href="#main-slider" data-slide="next">
        <i class="icon-angle-right"></i>
    </a>
</section><!--/#main-slider-->
