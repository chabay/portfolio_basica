{{--
  ./resources/views/projets/show.blade.php
  variables disponibles :
      - $projet Projet
      - $projetsSimilaires array(Projet)
 --}}
@extends('template.defaut')

@section('title')
  {{ $projet->titre }}
@endsection

@section('content1')
 <!-- Page Title -->
 <div class="section section-breadcrumbs">
 <div class="container">
 <div class="row">
   <div class="col-md-12">
     <h1>Product Details</h1>
   </div>
 </div>
 </div>
 </div>

 <div class="section">
 <div class="container">
   <div class="row">
     <!-- Product Image & Available Colors -->
     <div class="col-sm-6">
       <div class="product-image-large">
         <img src="{{ asset('img/portfolio/' . $projet->image) }}" alt="{{ $projet->titre }}">
       </div>
       <div class="colors">
       <span class="color-white"></span>
       <span class="color-black"></span>
       <span class="color-blue"></span>
       <span class="color-orange"></span>
       <span class="color-green"></span>
     </div>
     </div>
     <!-- End Product Image & Available Colors -->
     <!-- Product Summary & Options -->
     <div class="col-sm-6 product-details">
       <h2>{{ $projet->titre }}</h2>
       <h3>Quick Overview</h3>
       <p>{{ $projet->description }}</p>
     <h3>Project Details</h3>
     <p><strong>Client: </strong> {{ $projet->client }}</p>
     <p><strong>Date: </strong>{{ \Carbon\Carbon::parse($projet->created_at)->format('F d, Y') }}</p>
     <p><strong>Tags: </strong>
       <ul>
         @foreach ($projet->tags as $tag)
         <li>
              <a href="{{ route('tags.show', [
                 'tag' => $tag->id,
                 'slug' => Str::slug($tag->nom)
               ]) }}">
               {{ $tag->nom }}
             </a>
           </li>
         @endforeach
       </ul>
     </p>
     </div>
     <!-- End Product Summary & Options -->

   </div>
 </div>
 </div>

 <hr>

 <div class="section">
 <div class="container">
 <div class="row">

 <div class="section-title">
 <h1>Similar Works</h1>
 </div>


 <ul class="grid cs-style-2">
   @foreach ($projetsSimilaires as $projetSimilaire)
     <div class="col-md-3 col-sm-6">
       <figure>
         <img src="{{ asset('img/portfolio/' . $projetSimilaire->image) }}" alt="{{ $projetSimilaire->titre }}">
         <figcaption>
           <h3>{{ Str::limit($projetSimilaire->titre, 8, '...') }}</h3>
           <span>{{ Str::limit($projetSimilaire->client, 15, '...') }}</span>
           <a href="{{ route('projets.show', [
              'projet' => $projetSimilaire->id,
              'slug' => Str::slug($projetSimilaire->titre)
            ]) }}">Take a look</a>
         </figcaption>
       </figure>
     </div>
   @endforeach
 </ul>


 </div>
 </div>
 </div>
@endsection
