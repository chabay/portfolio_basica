{{--
  ./resources/views/posts/latest.blade.php
  variables disponibles :
      - $posts array(Post)
 --}}


    <!-- Featured News -->
    <div class="col-sm-6 featured-news">
      <h2>Latest Blog Posts</h2>
      @foreach ($posts as $post)
        <div class="row">
          <div class="col-xs-4">
            <a href="{{ route('posts.show', [
                        'post' => $post->id,
                        'slug' => Str::slug($post->titre)
                        ]) }}">
              <img src="{{ asset('img/blog/' . $post->image) }}" alt="{{ $post->titre }}">
            </a>
          </div>
          <div class="col-xs-8">
            <div class="caption"><a href="{{ route('posts.show', [
                        'post' => $post->id,
                        'slug' => Str::slug($post->titre)
                        ]) }}">{{ $post->titre }}</a></div>
            <div class="date">{{ \Carbon\Carbon::parse($post->created_at)->format('d F Y') }}</div>
            <div class="intro">{{ Str::words($post->texte, 15, '...') }}
              <a href="{{ route('posts.show', [
                          'post' => $post->id,
                          'slug' => Str::slug($post->titre)
                          ]) }}">
                Read more...
              </a>
            </div>
          </div>
        </div>
      @endforeach
    </div>
    <!-- End Featured News -->
