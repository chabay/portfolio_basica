{{--
  ./resources/views/posts/latest.blade.php
  variables disponibles :
      - $posts array(Post)
 --}}

 {!! '<'.'?'.'xml version="1.0" encoding="UTF-8" ?>' !!}

 <rss version="2.0">
   <channel>

        <title>Basica!</title>
        <link>http://basica.com</link>
        <description>Latest posts from Basica</description>
        <language>en</language>
        <lastBuildDate>{{ $posts[0]->created_at->format(DateTime::RSS) }}</lastBuildDate>

        @foreach ($posts as $post)
                <item>
                    <title>{{ $post->titre }}</title>
                    <link>{{ route('posts.show', [
                                'post' => $post->id,
                                'slug' => Str::slug($post->titre)
                                ]) }}</link>
                    <guid isPermaLink="true">{{ route('posts.show', [
                                'post' => $post->id,
                                'slug' => Str::slug($post->titre)
                                ]) }}</guid>
                    <description>{{ Str::words($post->texte, 100, '...') }}</description>
                    <pubDate>{{ $post->created_at->format(DateTime::RSS) }}</pubDate>
                </item>
        @endforeach

    </channel>
</rss>
