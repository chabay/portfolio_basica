{{--
  ./resources/views/posts/show.blade.php
  variables disponibles :
      - $post Post
      - $posts array(Post) (latest posts)
 --}}

 @extends('template.defaut')

 @section('title')
   {{ $post->titre }}
 @endsection

 @section('content1')

  <!-- Page Title -->
    <div class="section section-breadcrumbs">
     <div class="container">
       <div class="row">
         <div class="col-md-12">
           <h1>Blog Post</h1>
         </div>
       </div>
     </div>
    </div>

   <div class="section">
     <div class="container">
       <div class="row">
         <!-- Blog Post -->
         <div class="col-sm-8">
           <div class="blog-post blog-single-post">
             <div class="single-post-title">
               <h2>{{ $post->titre }}</h2>
             </div>

             <div class="single-post-image">
               <img src="{{ asset('img/blog/' . $post->image) }}" alt="{{ $post->titre }}">
             </div>
             <div class="single-post-info">
               <i class="glyphicon glyphicon-time"></i>{{ \Carbon\Carbon::parse($post->created_at)->format('d M, Y') }}
               <a href="#" title="Show Comments"><i class="glyphicon glyphicon-comment"></i>11</a>
             </div>
             <div class="single-post-info">
               Category : <a href="{{ route('categories.show', [
                           'categorie' => $post->categorie->id,
                           'slug' => Str::slug($post->categorie->nom)
                           ]) }}">
                              {{ $post->categorie->nom }}
                          </a>
             </div>
             <div class="single-post-content">
               <p>
                 {{ $post->texte }}
               </p>
             </div>
           </div>
         </div>
         <!-- End Blog Post -->
         <!-- Sidebar -->
         <div class="col-sm-4 blog-sidebar">

           <h4>Recent Posts</h4>
           <ul class="recent-posts">
             @foreach ($posts as $post)
               <li><a href="{{ route('posts.show', [
                           'post' => $post->id,
                           'slug' => Str::slug($post->titre)
                           ]) }}">
                           {{ $post->titre }}
               </a></li>
             @endforeach
           </ul>
           <h4>Categories</h4>
           @include('categories._index')

         </div>
         <!-- End Sidebar -->
       </div>
     </div>
   </div>

 @endsection
