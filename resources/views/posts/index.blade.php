{{--
  ./resources/views/posts/index.blade.php
  variables disponibles :
      - $posts array(Post)
 --}}


<div class="section">
  <div class="container">
    <div class="row">
      @foreach ($posts as $post)
        <!-- Blog Post Excerpt -->
        <div class="col-sm-6">
          <div class="blog-post blog-single-post">
            <div class="single-post-title">
              <h2>{{ $post->titre }}</h2>
            </div>

            <div class="single-post-image">
              <img src="{{ asset('img/blog/' . $post->image) }}" alt="{{ $post->titre }}">
            </div>

            <div class="single-post-info">
              <i class="glyphicon glyphicon-time"></i>{{ \Carbon\Carbon::parse($post->created_at)->format('d M, Y') }}
              <a href="#" title="Show Comments"><i class="glyphicon glyphicon-comment"></i>11</a>
            </div>
            <div class="single-post-info">
              Category : <a href="{{ route('categories.show', [
                          'categorie' => $post->categorie->id,
                          'slug' => Str::slug($post->categorie->nom)
                          ]) }}">
                             {{ $post->categorie->nom }}
                         </a>
            </div>

            <div class="single-post-content">
              <p>
                {{ Str::words($post->texte, 30, '...') }}
              </p>
              <a href="{{ route('posts.show', [
                          'post' => $post->id,
                          'slug' => Str::slug($post->titre)
                        ]) }}" class="btn">
                  Read more
              </a>
            </div>
          </div>
        </div>
        <!-- End Blog Post Excerpt -->
      @endforeach

        <!-- Pagination -->
        <div class="pagination-wrapper col-sm-12">
          <ul class="pagination pagination-sm">
            {!! $posts->links(); !!}
          </ul>
        </div>
      </div>
  </div>
</div>
