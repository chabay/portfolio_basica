{{--
  ./resources/views/categories/show.blade.php
  variables disponibles :
      - $categorie Categorie
      - $posts array(Post)
 --}}

 @extends('template.defaut')

 @section('title')
   Category : {{ $categorie->nom }}
 @endsection

 @section('content1')

  <!-- Page Title -->
    <div class="section section-breadcrumbs">
     <div class="container">
       <div class="row">
         <div class="col-md-12">
           <h1>Category : {{ $categorie->nom }}</h1>
         </div>
       </div>
     </div>
    </div>

 @include('posts.index')

 @endsection
