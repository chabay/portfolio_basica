{{--
  ./resources/views/pages/_index.blade.php
  variables disponibles :
      - $categories array(Categorie)
 --}}

<ul class="blog-categories">
  @foreach ($categories as $categorie)
     <li><a href="{{ route('categories.show', [
                'categorie' => $categorie->id,
                'slug' => Str::slug($categorie->nom)
                  ]) }}">
        {{ $categorie->nom }}
    </a></li>
  @endforeach
</ul>
