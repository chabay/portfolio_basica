{{--
  ./resources/views/tags/show.blade.php
  variables disponibles :
      - $tag Tag
      - $projets array(Projet)
 --}}

  @extends('template.defaut')

  @section('title')
    Tag : {{ $tag->nom }}
  @endsection

  @section('content1')

   <!-- Page Title -->
     <div class="section section-breadcrumbs">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1>Tag : {{ $tag->nom }}</h1>
          </div>
        </div>
      </div>
     </div>

     @include('projets.index')

   @endsection
