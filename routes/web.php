<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// ROUTE PAR DEFAUT
// PATTERN : /
// CTRL : PagesController
// ACTION : Show

  Route::get('/', 'PagesController@show')->defaults('page', 1)->name('homepage');

// ROUTE DU DETAIL D'UNE PAGE
// PATTERN : /pages/id/slug.html
// CTRL : PagesController
// ACTION : Show

  Route::get('/pages/{page}/{slug}.html', 'PagesController@show')
  ->where(['page' => '[1-9][0-9]*',
           'slug' => '[a-z0-9]+[a-z0-9\-]*[a-z0-9]'])
  ->name('pages.show');

// ROUTE DES PROJETS

  Route::prefix('projets')->name('projets.')->group(function () {

  // ROUTE DU DETAIL D'UN PROJET
  // PATTERN : /projets/id/slug.html
  // CTRL : ProjetsController
  // ACTION : Show

    Route::get('{projet}/{slug}.html', 'ProjetsController@show')
    ->where(['projet' => '[1-9][0-9]*',
             'slug' => '[a-z0-9]+[a-z0-9\-]*[a-z0-9]'])
    ->name('show');

  // ROUTE AJAX DU CHARGEMENT DES ANCIENS PROJETS
  // PATTERN : /projets/older.html
  // CTRL : ProjetsController
  // ACTION : ajaxOlder

    Route::get('older.html', 'ProjetsController@ajaxOlder')->name('ajax.older');
  });


// ROUTE DES POSTS

  Route::prefix('posts')->name('posts.')->group(function () {

  // ROUTE DU DETAIL D'UN POST
  // PATTERN : /posts/id/slug.html
  // CTRL : PostsController
  // ACTION : Show

    Route::get('{post}/{slug}.html', 'PostsController@show')
    ->where(['post' => '[1-9][0-9]*',
             'slug' => '[a-z0-9]+[a-z0-9\-]*[a-z0-9]'])
    ->name('show');

  // ROUTE DU FLUX RSS DES POSTS
  // PATTERN : /posts/rss.html
  // CTRL : PostsController
  // ACTION : rss

    Route::get('rss', 'PostsController@feed')->name('rss');
  });

// ROUTE DU DETAIL D'UNE CATEGORIE
// PATTERN : /categories/id/slug.html
// CTRL : CategoriesController
// ACTION : Show

  Route::get('/categories/{categorie}/{slug}.html', 'categoriesController@show')
  ->where(['categorie' => '[1-9][0-9]*',
           'slug' => '[a-z0-9]+[a-z0-9\-]*[a-z0-9]'])
  ->name('categories.show');

// ROUTE DU DETAIL D'UN TAG
// PATTERN : /tags/id/slug.html
// CTRL : TagsController
// ACTION : Show

   Route::get('/tags/{tag}/{slug}.html', 'TagsController@show')
   ->where(['tag' => '[1-9][0-9]*',
           'slug' => '[a-z0-9]+[a-z0-9\-]*[a-z0-9]'])
   ->name('tags.show');
