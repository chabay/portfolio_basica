<?php
//  ./app/Http/Composers/CategoriesComposer.php

namespace App\Http\Composers;

use Illuminate\View\View;
use \App\Models\Categorie;

class CategoriesComposer {
    /**
     * @param  View  $view
     * @return void
     */
    public function compose(View $view) {
        $view->with('categories', Categorie::all());
    }

    /**
     * @param  View  $view
     * @return void
     */
    public function index(View $view) {
        $view->with('categories', Categorie::orderBy('nom', 'ASC')->get());
    }
}
