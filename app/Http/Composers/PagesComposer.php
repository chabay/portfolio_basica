<?php
//  ./app/Http/Composers/PagesComposer.php

namespace App\Http\Composers;

use Illuminate\View\View;
use \App\Models\Page;

class PagesComposer {
    /**
     * @param  View  $view
     * @return void
     */
    public function compose(View $view) {
        $view->with('pages', Page::all());
    }

    /**
     * @param  View  $view
     * @return void
     */
    public function menu(View $view) {
        $view->with('pages', Page::all());
    }
}
