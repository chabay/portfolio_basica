<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Models\Page;
use App\Models\Projet;
use App\Models\Post;
use Twitter;

class PagesController extends Controller
{
  /**
   * Affiche le détail d'une page
   * @param  Page   $page
   * @return vue pages.show
   */
   public function show(Page $page) {
     if($page->id === 1):
       $projets = Projet::latest()->take(6)->get();
       $slides = Projet::where('slider', '=', '1')->get();
       $posts = Post::latest()->take(3)->get();
       $tweets = Twitter::getUserTimeline(['screen_name' => 'zaschabay', 'count' => 3, 'format' => 'object']);
       return View::make('pages.show', compact('page', 'projets', 'posts', 'slides', 'tweets'));

     elseif($page->id === 2):
       $projets = Projet::latest()->take(6)->get();
       return View::make('pages.show', compact('page', 'projets'));

     elseif($page->id === 3):
       $posts = Post::latest()->paginate(4);
       return View::make('pages.show', compact('page', 'posts'));

     else:
      return View::make('pages.show', compact('page'));

     endif;
   }
}
