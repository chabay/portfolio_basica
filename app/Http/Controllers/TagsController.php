<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Models\Tag;

class TagsController extends Controller
{
  /**
   * Affiche le détail d'un tag
   * @param  Tag   $tag
   * @return vue tags.show
   */
   public function show(Tag $tag) {
      $projets = $tag->projets()->latest()->get();
      return View::make('tags.show', compact('tag', 'projets'));
   }
}
