<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PageRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PageCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PageCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Page');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/page');
        $this->crud->setEntityNameStrings('page', 'pages');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        //$this->crud->setFromDb();
        $this->crud->orderBy('id', 'ASC');
        //Enlève le bouton preview
        $this->crud->removeButton('show');

        $this->crud->addColumn([
          'name' => 'titrePage', // The db column name
          'label' => "Titre de la page", // Table column heading
          'type' => 'Text'
        ]);

        $this->crud->addColumn([
          'name' => 'titreMenu', // The db column name
          'label' => "Titre dans le menu", // Table column heading
          'type' => 'Text'
        ]);

        $this->crud->addColumn([
          'name' => 'titreContenu', // The db column name
          'label' => "Titre du contenu", // Table column heading
          'type' => 'Text'
        ]);

        $this->crud->addColumn([
          'name' => 'sousTitreContenu', // The db column name
          'label' => "Sous-titre du contenu", // Table column heading
          'type' => 'Text'
        ]);

        $this->crud->addColumn([
          'name' => 'texte', // The db column name
          'label' => "Texte", // Table column heading
          'type' => 'Text'
        ]);

    }


    protected function setupCreateOperation()
    {
        $this->crud->setValidation(PageRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        //$this->crud->setFromDb();

        $this->crud->addField([
          'name' => 'titrePage', // The db column name
          'label' => "Titre de la page", // Table column heading
          'type' => 'Text'
        ]);

        $this->crud->addField([
          'name' => 'titreMenu', // The db column name
          'label' => "Titre dans le menu", // Table column heading
          'type' => 'Text'
        ]);

        $this->crud->addField([
          'name' => 'titreContenu', // The db column name
          'label' => "Titre du contenu", // Table column heading
          'type' => 'Text'
        ]);

        $this->crud->addField([
          'name' => 'sousTitreContenu', // The db column name
          'label' => "Sous-titre du contenu", // Table column heading
          'type' => 'Text'
        ]);

        $this->crud->addField([
          'name' => 'texte', // The db column name
          'label' => "Texte", // Table column heading
          'type' => 'Textarea'
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
