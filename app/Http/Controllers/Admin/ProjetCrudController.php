<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProjetRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ProjetCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProjetCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Projet');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/projet');
        $this->crud->setEntityNameStrings('projet', 'projets');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();

        //Enlève le bouton preview
        $this->crud->removeButton('show');

        //Affiche les tags
        $this->crud->addColumn([
          // n-n relationship / montre les tags d'un article
          'label' => "Tags", // Table column heading
          'type' => "select_multiple",
          'name' => 'tags', // the method that defines the relationship in your Model
          'entity' => 'tags', // the method that defines the relationship in your Model
          'attribute' => "nom", // foreign key attribute that is shown to user
          'model' => "App\Models\Tag", // foreign key model
        ]);

        $this->crud->removeColumn('slider');
        $this->crud->addColumn([
        'name' => 'slider',
        'label' => 'Affiché dans le slider',
        'type' => 'boolean',
        // optionally override the Yes/No texts
        'options' => [0 => 'Non', 1 => 'Oui']
        ]);

        //Affiche l'image
        $this->crud->removeColumn('image');
        $this->crud->addColumn([
          'name' => 'image', // The db column name
          'label' => "Image", // Table column heading
          'type' => 'image',
          'prefix' => 'img/portfolio/',
          'height' => '200px',
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ProjetRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();

        //Ajout de tags
        $this->crud->addField([
          'type' => 'select2_multiple',
          'name' => 'tags', // the relationship name in your Model
          'entity' => 'tags', // the relationship name in your Model
          'attribute' => 'nom', // attribute on Article that is shown to admin
          'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
          'options'   => (function ($query) {
              return $query->orderBy('nom', 'ASC')->get();
           }),
         ]);

        //Option d'affichage dans le slider
        $this->crud->removeField('slider');
        $this->crud->addField([
          'name' => 'slider',
          'label' => 'Afficher dans le slider',
          'type' => 'checkbox'
        ]);

        //Ajout d'une image
        $this->crud->removeField('image');
        $this->crud->addField([
          'label' => "Image",
          'name' => "image",
          'type' => 'image',
          'upload' => true,
          'crop' => false, // set to true to allow cropping, false to disable
          'aspect_ratio' => 1,
          'prefix' => 'img/portfolio/',
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
