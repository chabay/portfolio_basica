<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PostRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PostCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PostCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Post');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/post');
        $this->crud->setEntityNameStrings('post', 'posts');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();

        //Enlève le bouton preview
        $this->crud->removeButton('show');

        //Affiche la catégorie
        $this->crud->removeColumn('categorie_id');
        $this->crud->addColumn([
          // 1-n relationship catégorie / montre la catégorie d'un article
          'label' => "Catégorie", // Table column heading
          'type' => "select",
          'name' => 'categorie_id', // the column that contains the ID of that connected entity;
          'entity' => 'categorie', // the method that defines the relationship in your Model
          'attribute' => "nom", // foreign key attribute that is shown to user
          'model' => "App\Models\Categorie", // foreign key model
        ]);

        //Affiche l'image
        $this->crud->removeColumn('image');
        $this->crud->addColumn([
          'name' => 'image', // The db column name
          'label' => "Image", // Table column heading
          'type' => 'image',
          'prefix' => 'img/blog/',
          'height' => '200px',
        ]);

    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(PostRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();

        //Ajout d'une catégorie
        $this->crud->removeField('categorie_id');
        $this->crud->addField([
         'label' => 'Catégorie',
         'type' => 'select',
         'name' => 'categorie_id', // the db column for the foreign key
         'entity' => 'categorie', // the method that defines the relationship in your Model
         'attribute' => 'nom', // foreign key attribute that is shown to user
         'model' => "App\Models\Categorie",
         'options'   => (function ($query) {
           return $query->orderBy('nom', 'ASC')->get();
          }),
        ]);

        //Ajout d'une image
        $this->crud->removeField('image');
        $this->crud->addField([
          'label' => "Image",
          'name' => "image",
          'type' => 'image',
          'upload' => true,
          'crop' => false, // set to true to allow cropping, false to disable
          'aspect_ratio' => 1,
          'prefix' => 'img/blog/',
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
