<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Models\Projet;
use App\Models\Tag;

class ProjetsController extends Controller
{
  /**
   * Affiche le détail d'une projet
   * @param  Projet   $projet
   * @return vue projets.show
   */
   public function show(Projet $projet) {

      $tagsIds = $projet->tags->pluck('id')->toArray();
      //Récupère les projets similaires
      $projetsSimilaires = Projet::whereHas('tags', function ($query) use ($tagsIds) {
          return $query->whereIn('id', $tagsIds);
        })->where('id', '!=', $projet->id)
          ->get()
          ->sortByDesc(function($projetSimilaire, $key) use ($tagsIds) {
            $intersections = $projetSimilaire->tags->pluck('id')->intersect($tagsIds);
            return count($intersections);
          })
          ->take(4);

      return View::make('projets.show', compact('projet', 'projetsSimilaires'));
   }


   public function ajaxOlder(Request $request) {
     $projets = Projet::latest()
                        ->take(6)
                        ->offset($request->get('offset'))
                        ->get();

     return View::make('projets.list', compact('projets'));

   }
}
