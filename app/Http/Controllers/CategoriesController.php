<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Models\Categorie;

class CategoriesController extends Controller
{
  /**
   * Affiche le détail d'une catégorie
   * @param  Categorie   $categorie
   * @return vue categories.show
   */
   public function show(Categorie $categorie) {
      //Récupère les derniers posts de la catégorie
      $posts = $categorie->posts()->latest()->paginate(4);
      return View::make('categories.show', compact('categorie', 'posts'));
   }
}
