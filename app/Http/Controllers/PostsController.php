<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Models\Post;

class PostsController extends Controller
{

  /**
   * Affiche le détail d'un post
   * @param  Post   $post
   * @return vue posts.show
   */
   public function show(Post $post) {
      //Récupère les derniers posts
      $posts = Post::latest()->take(4)->get();
      return View::make('posts.show', compact('post', 'posts'));
   }

   /**
    * Retourne le flux rss des posts
    * @return fichier xml
    */
   public function feed() {
     $posts = Post::latest()->get();
     return response()->view('posts.rss', compact('posts'))->header('Content-Type', 'text/xml');
   }
}
