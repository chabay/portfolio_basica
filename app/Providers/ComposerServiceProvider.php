<?php
// ./app/Providers/ComposerServiceProvider.php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider {
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
          View::composer('pages._menu', '\App\Http\Composers\PagesComposer@menu');
          View::composer('categories._index', '\App\Http\Composers\CategoriesComposer@index');
    }
}
