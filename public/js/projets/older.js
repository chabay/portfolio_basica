/*
  ./js/projets/older.js
 */
//Chargement des anciens projets
$(function(){
  let offset = 6;
  let baseUrl = $('body').attr('data-baseUrl');
  $('#load-more').click(function(e){
    e.preventDefault();
    $.ajax({
      url: baseUrl + '/projets/older.html',
      data: {
        offset: offset
      },
      method: 'get',
      context: this
    })
     .done(function(reponsePHP){
       $('#liste-projets').append(reponsePHP)
                        .find('.projet:nth-last-of-type(-n+6)')
                        .hide()
                        .fadeIn();
       offset += 6;
     })
     .fail(function(){
       alert('Problème durant la transaction');
     });
    });
});
