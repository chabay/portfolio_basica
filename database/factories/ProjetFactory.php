<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Projet;
use Faker\Generator as Faker;

$factory->define(Projet::class, function (Faker $faker) {
    return [
      'titre' => $faker->sentence(2),
      'image' => $faker->numberBetween(1, 8),
      'description' => $faker->text(200),
      'client' => $faker->company,
    ];
});
