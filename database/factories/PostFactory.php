<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'titre' => $faker->sentence(5),
        'image' => $faker->numberBetween(1, 4),
        'texte' => $faker->text(200),
        'categorie_id' => $faker->numberBetween(1, 10),
    ];
});
